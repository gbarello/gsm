import numpy as np
import utilities as utils

def get_model_data(direc):

    fac = np.loadtxt(direc + "/fac.csv")
    log = np.loadtxt(direc + "/train_log.csv")
    param = utils.read_dict(direc + "/parameters")
    paths = utils.fetch_file(direc + "/paths.pkl")
    segs = utils.fetch_file(direc + "/segs.pkl")
    kernels = utils.fetch_file(direc + "/kernels.pkl")

    C = utils.fetch_file(direc + "/C.pkl")
    Q = utils.fetch_file(direc + "/Q.pkl")
    F = utils.fetch_file(direc + "/F.pkl")
    P = utils.fetch_file(direc + "/P.pkl")

    return {"fac":fac,"params":param,"paths":paths,"kernels":kernels,"C":C,"Q":Q,"F":F,"P":P,"segs":segs,"log":log}

def flat(x):
    return np.reshape(x,[-1]).tolist()

def get_f_pos(pos_type,fdist,nsur):

    if pos_type == "default":
        return [[0,0]] + [[fdist*np.cos(a),fdist*np.sin(a)] for a in np.linspace(0,np.pi*2,nsur+1)][:-1]

def get_segmentation(seg_type,nang,wave,fpos):

    if seg_type == "gsm":
        #only 1 seg. All the finters are in it.
        indices = [[[i for i in range(nang*len(wave)*len(fpos)*2)]]]  
        return indices
    
    elif seg_type == "default":
        #the default (CC) is to have a non-shared seg (center separate from surrounds) plus one seg for each angle, with the center shared with the surround of that angle, and other amgles separate.

        
        indices = []
        
        i = 0
        for p in fpos:
            for a in range(nang):
                for w in wave:
                    for t in range(2):
                        indices.append(i)
                        i+=1

        indices = np.reshape(np.array(indices),[len(fpos),nang,len(wave),2])

        seg = []

        seg.append([flat(indices[0])] + [flat(indices[1:,i]) for i in range(nang)])
        
        for i in range(nang):
            seg.append([flat(indices[0]) + flat(indices[1:,i])] + [flat(indices[1:,k]) for k in range(nang) if k != i])

        return seg

if __name__ == "__main__":
    if 0:
        from GSM.MGSM_inference import check_seg
    
        A = get_segmentation("default",4,[16],[i for i in range(9)])
        
        for a in A:
            print(len([x for B in a for x in B]))
            
        print(check_seg(A))

    else:
        print(get_f_pos("default",10,8))
        print(len(get_f_pos("default",10,8)))
