#!/bin/bash

sbatch run_GSM_responses.sh 1. 7
sbatch run_GSM_responses.sh .75 10
sbatch run_GSM_responses.sh .5 14
sbatch run_GSM_responses.sh .25 28
sbatch run_GSM_responses.sh .2 35

#python general_GSM_responses.py model_files/model_file_18/ ori_tuning --TA=100 --dt=2. --n_frame=5
#python general_GSM_responses.py model_files/model_file_18/ ori_tuning --TA=100 --dt=1.5 --n_frame=7
#python general_GSM_responses.py model_files/model_file_18/ ori_tuning --TA=100 --dt=1. --n_frame=10
#python general_GSM_responses.py model_files/model_file_18/ ori_tuning --TA=100 --dt=.75 --n_frame=13
#python general_GSM_responses.py model_files/model_file_18/ ori_tuning --TA=100 --dt=.5 --n_frame=20
#python general_GSM_responses.py model_files/model_file_18/ ori_tuning --TA=100 --dt=.25 --n_frame=40

#sbatch run_GSM_responses.sh .15 47
#sbatch run_GSM_responses.sh .2 35
