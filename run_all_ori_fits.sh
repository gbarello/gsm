#!/bin/bash

sbatch run_GSM_responses.sh .05 15
sbatch run_GSM_responses.sh .1 7
sbatch run_GSM_responses.sh .15 5
sbatch run_GSM_responses.sh .2 4
sbatch run_GSM_responses.sh .25 3
