#!/bin/bash
#SBATCH --partition=short
#SBATCH --time=0-02:00:00
#SBATCH --ntasks-per-node 24

module load anaconda2
source activate theano_GPU
cd /projects/ahmadianlab/gbarello/research/gsm

#$1 is model file 
#$2 is response type (ori_tuning or size_tuning)
#$3 is dt
#$4 is snr
#$5 is nf

python general_GSM_responses.py $1 $2 --n_frame=$5 --snr=$4 --npnt=20 --dt=$3 --con=$6
python general_GSM_responses.py $1 $2 --n_frame=$5 --snr=$4 --npnt=20 --dt=$3 --con=$6 --variance
