#!/bin/bash
#SBATCH --partition=long
#SBATCH --time=5-00:00:00
#SBATCH --ntasks-per-node 24

module load anaconda2
source activate theano_GPU
cd /projects/ahmadianlab/gbarello/research/gsm

#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=.5 --TA=50 --npnt=20 --dt=.2 
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=.5 --TA=50 --npnt=20 --dt=.2 --variance

#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=.5 --npnt=20 --dt=.2 
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=.5 --npnt=20 --dt=.2 --variance

#python general_GSM_responses.py ./model_files/model_file_16/ ori_tuning --n_frame=5 --snr=.5 --TA=50 --npnt=20 --dt=.2
#python general_GSM_responses.py ./model_files/model_file_16/ ori_tuning --n_frame=5 --snr=.5 --TA=50 --npnt=20 --dt=.2
python general_GSM_responses.py ./model_files/model_file_20/ ori_tuning --n_frame=5 --snr=.5 --npnt=20 --dt=.2


#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=20 --snr=.1 --npnt=20 --dt=.05
#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=20 --snr=.5 --npnt=20 --dt=.05
#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=20 --snr=.75 --npnt=20 --dt=.05
#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=20 --snr=1. --npnt=20 --dt=.05#

#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=2 --snr=.1 --npnt=20 --dt=.5
#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=2 --snr=.5 --npnt=20 --dt=.5
#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=2 --snr=.75 --npnt=20 --dt=.5
#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --n_frame=2 --snr=1. --npnt=20 --dt=.5

#python general_GSM_responses.py ./model_files/model_file_20/ ori_tuning --n_frame=5 --snr=.5 --TA=50 --npnt=20 --dt=.2

#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=.1 --TA=50 --npnt=20
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=.5 --TA=50 --npnt=20
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=1. --TA=50 --npnt=20
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=1.5 --TA=50 --npnt=20
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=2. --TA=50 --npnt=20
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=10. --TA=50 --npnt=20

#python general_GSM_responses.py ./model_files/model_file_16/ ori_tuning --n_frame=10 --snr=0.1 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_16/ ori_tuning --n_frame=10 --snr=0.25 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_16/ ori_tuning --n_frame=10 --snr=0.75 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_16/ ori_tuning --n_frame=10 --snr=1.0 --TA=50 --npnt=20 --dt=.25

#python general_GSM_responses.py ./model_files/model_file_17/ ori_tuning --n_frame=10 --snr=0.1 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_17/ ori_tuning --n_frame=10 --snr=0.25 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_17/ ori_tuning --n_frame=10 --snr=0.75 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_17/ ori_tuning --n_frame=10 --snr=1.0 --TA=50 --npnt=20 --dt=.25

#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=0.1 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=0.25 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=0.75 --TA=50 --npnt=20 --dt=.25
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=1.0 --TA=50 --npnt=20 --dt=.25

#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=0.1 --TA=50 --npnt=20 --dt=.25 --variance
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=0.25 --TA=50 --npnt=20 --dt=.25 --variance
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=0.75 --TA=50 --npnt=20 --dt=.25 --variance
#python general_GSM_responses.py ./model_files/model_file_18/ ori_tuning --n_frame=10 --snr=1.0 --TA=50 --npnt=20 --dt=.25 --variance


#python general_GSM_responses.py ./model_files/model_file_26/ size_tuning --dt=${1} --n_frame=${2} --TA=50 --npnt=20 --variance

#python general_GSM_responses.py ./model_files/model_file_18/ MI
#python general_GSM_responses.py ./model_files/model_file_18/ nat_MI
