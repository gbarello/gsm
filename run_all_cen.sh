#!/bin/bash


ARRAY=(BSDS_8_8_24_8_4_2_8_8 BSDS_8_8_24_8_8_2_8_8)
BARRAY=(1 2 3 4 5 6 7 8 9 10)

for j in ${ARRAY[@]}; do
    for k in ${BARRAY[@]};do
	sbatch crunfile.srun $j True True $k
    done
done
