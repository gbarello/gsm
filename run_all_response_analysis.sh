#!/bin/bash

module load python2

for d in ./inference/visualGSM/*; do
    sbatch run_response_analysis.srun $d
done
