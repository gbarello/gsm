#!/bin/bash

ARRAY=(BSDS_8_8_24_8_4_2_8_8 BSDS_16_16_48_8_4_2_16_16)

MARRAY=(1 2 3 4 5 6 7 8 9 10)

for i in ${MARRAY[@]}; do
    for j in ${ARRAY[@]}; do
	sbatch varrunfile.srun $j coen_cagli True True $i
    done
done

