#!/bin/bash

rm *.out

#ARRAY=(BSDS_8_8_24_8_4_2_8_8 BSDS_8_8_24_8_4_2_10_10 BSDS_8_8_24_8_4_2_12_12 BSDS_16_16_48_8_4_2_16_16 BSDS_16_16_48_8_4_2_20_20 BSDS_16_16_48_8_4_2_24_24)
ARRAY=(BSDS_20_20_60_8_4_2_10_10)

MARRAY=(coen_cagli)

BARRAY=(True)

for i in ${MARRAY[@]}; do
    for j in ${ARRAY[@]}; do
	for k in ${BARRAY[@]}; do
	    echo $i
#	    sbatch vrunfile.srun $j $i $k False
	done
    done
done

CARRAY=(BSDS_8_8_24_8_4_2_8_8)
#(BSDS_8_8_24_8_8_2_8_8)
#CARRAY=(BSDS_8_8_24_8_16_2_8_8)

for j in ${CARRAY[@]}; do
    for k in ${BARRAY[@]};do
	echo $j
	sbatch crunfile.srun $j $k False
    done
done
