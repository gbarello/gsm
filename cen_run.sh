#!/bin/bash

cd research/GSM

module load anaconda2
source activate theano_GPU


python fit_central_model.py $1
