#!/bin/bash

#$1 is model file 
#$2 is response type (ori_tuning or size_tuning)
#$3 is nframe
#$4 is snr
#$5 is dt

nf=( 2 5 10 20 )
dt=( ".5" ".2" ".1" ".05" )

for i in ".1" ".5" ".75" "1." "2."
do
    for j in 0 1 2 3
    do
	for c in 0 1
	do
	    #echo "${dt[$j]} $i ${nf[$j]}"
	    #sbatch ./run_single_att_response.sh ./model_files/model_file_16/ ori_tuning ${dt[$j]} $i ${nf[$j]} $c
	    #sbatch ./run_single_att_response.sh ./model_files/model_file_17/ ori_tuning ${dt[$j]} $i ${nf[$j]} $c
	    #sbatch ./run_single_att_response.sh ./model_files/model_file_18/ ori_tuning ${dt[$j]} $i ${nf[$j]} $c
	    #sbatch ./run_single_att_response.sh ./model_files/model_file_20/ ori_tuning ${dt[$j]} $i ${nf[$j]} $c
	    
	    #sbatch ./run_single_att_response.sh ./model_files/model_file_26/ size_tuning ${dt[$j]} $i ${nf[$j]} $c
	    
	    sbatch ./run_single_att_response.sh ./model_files/MGSM_model_file_3/ size_tuning ${dt[$j]} $i ${nf[$j]} $c
	done
    done
done

